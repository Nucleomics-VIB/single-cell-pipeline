FROM continuumio/miniconda3:latest

# Install procps so that Nextflow can poll CPU usage and
# deep clean the apt cache to reduce image/layer size
RUN apt-get update \
      && apt-get install -y procps \
      && apt-get clean -y && rm -rf /var/lib/apt/lists/*

# get the environment file
COPY environment.yml environment.yml

# create the conda environment
RUN conda env create --name sckobe --file environment.yml && conda clean -a

# add conda isntallation dir to PATH instead of doing 'conda activate'
ENV PATH /opt/conda/envs/sckobe/bin:$PATH

