#!/usr/bin/env nextflow

// Script for running the complete sc10x pipeline. This script performs demultiplexing of raw run data
// from an illumina sequencer, creates QC plots and tables of the demultiplexing, Performs read mapping
// and counting using cellranger, creates QC table per project and creates a summary xlsx file per 
// project withs a summary of the read mapping and suggested re-pooling numbers

// Activate  the 2
nextflow.enable.dsl=2

/*
--------------- Configuration variables ---------------------
*/

/*
--------------- Default Parameters ---------------------
*/
// All following parameters are default parameters. They can be assigned different values on the 
// command line or in the configuration file if desired, but they don't need to be changed

// Following parameters specify if demultiplexing, read mapping and counting, and calculating re-pooling
// should be performed. Demultiplexing and mapping with cellranger can be performed separately or together 
params.demultiplexing = true
params.cellranger = false
params.repooling = false
params.nextcloud = false

// default sample sheet location. needed for demultiplexing process
params.samplesheet = "$launchDir/RawData/samplesheet"

// default samplefile location. Needed for cellrangercount process
// this parameter is the path to a csv file containing 4 columns: 
// Sample_Name, Sample_Project, Transcriptome and Expected_Cells
// each line of the file contains a sample name, it's corresponding project name, the path to the reference
// transcriptome and the number of expected cells in the sample (default 3000)
// The first lines of the file might look like this:
// 
// Sample_Name,Sample_Project,Transcriptome,Expected_Cells
// Basilicdot6_n1_DNA,3636test,/data/ResourceData/CellRanger/refdata-cellranger-mm10-1.2.0/,3000
// Basilicdot6_n2_DNA,3636test,/data/ResourceData/CellRanger/refdata-cellranger-mm10-1.2.0/,3000
// BH1_Visium_ADT,3659test,/data/ResourceData/CellRanger/refdata-cellranger-mm10-1.2.0/,4500
params.samplefile = "$launchDir/samples.csv"


// output directory. By default the launchdirectory is used but you can specify another one
params.outdir = "$launchDir"

// Default path for the directory containing the fastq files. This is needed for the cellrangercount process
// and the create_config process.
// The demultiplexing process puts the output files in this location by default. Change it if the fastq files
// are in another location 
params.fastqs_dir = "$params.outdir/RawData"


// Specify the path to the netrc-file. This file is needed when using the curl command in the nexctloud procees.
// This file is ideally a hiden file with only read and write permissions for the user. It's content should look like this:
// machine <external-machine-to-connect-too>
// login <login-email-adress>
// password <password>
params.netrcfile = "/home/kobe/testing/nextflow/.net-rc"

// settings for copying ToSend output to azure blob storage
params.azure = false
params.azstorage = "azure-storage-account-name"
params.azsastoken = "azure-sas-token"
params.azblob = "name-of-azure-blob-container"

/*
--------------- Command line Parameters ---------------------
*/
// The following parameters need to be specified on the command line or in the configuration file

//// assign config file ////
// A configuration file can be added on the command line, where parameters can be defined. The parameters
// defined in the config file take precedent over those defined in this script.
// Use the -params-file option to add the configuration file on the command line like in the following example
// -params-file nextflow_config.yml


//// Default params for Demultiplexing and poolingQC ////

params.barcode_mismatches = 1
params.create_fastq_for_index_reads = false

// Path to projectdir. Used by poolingQC.R to locate the library files 
params.directory = "$projectDir"
params.extra_parameters = ""

// If the fastq files are to be divided in project folders. This parameter is also needed in cellrangercount process
params.hassampleproject = true
params.lanes = "1,2"
params.mask_short_adapter_reads = true
params.nCPU = 12
params.runname = "runname"
params.runpath = "path/to/run/data/folder"
params.splitlanes_archive = false
params.splitlanes_fastq = true
params.splitprojectsarchive = true


//// Default params for cellrangerCount ////

// the number of threads to use in cellrangercount process
params.threads = 12

// If the fastq files are divided in project folders. This parameter is also needed in demultiplexing process
// params.hassampleproject = true

//// Default params for re-pooling ////
params.wanted_reads = 50000

//// make publishDir variables for ToStore and ToSend folders
params.tostoredir = params.outdir + "/ToStore"
params.tosenddir = params.outdir + "/ToSend"
params.logdir = params.tostoredir + "/log"

/*
--------------- Demultiplexing ---------------------
*/

process demultiplexing {
    // publishDir "Report/01_Demultiplexing/Stats", pattern: "*/DemultiplexingStats.xml", saveAs: {filename -> "DemultiplexingStats.xml"}, mode: "copy"
    // publishDir "Report/01_Demultiplexing/Logs", pattern: "demultiplexing_command.txt", mode: "copy"
    publishDir "$params.outdir/RawData", pattern: "**/*fastq.gz", mode: "move"
    publishDir "$params.outdir/RawData", pattern: "*fastq.gz", mode: "move"
    publishDir "$params.outdir/RawData", pattern: "Reports/**", mode: "copy"
    publishDir "$params.outdir/RawData", pattern: "Stats/**", mode: "copy"
    publishDir "${params.tostoredir}", pattern: "Reports/**", mode: "copy"
    publishDir "${params.tostoredir}", pattern: "Stats/**", mode: "copy"
    publishDir "${params.logdir}/${task.process}/${task.hash}", pattern: ".*", mode: "copy"

    
    input:
    file(samplesheet)
    path(runpath)

    output:
    stdout
    // file("${launchDir}/*/kikakommand.txt")
    val("Demultiplexing done"), emit: demultiplexing_out
    file("**")
    path("Stats/DemultiplexingStats.xml"), emit: demultiplexingstats
    path(".*")

    script:
    // validate splitlanes parameters
    if( params.splitlanes_archive && !params.splitlanes_fastq) {
        println "You cannot ask to spread the reads from different lanes over different archives if they are not in the first place spread over different files."
        params.splitlanes_archive = false
    }

    tiles_param = params.lanes? "--tiles=s_" + params.lanes.split(",").join(',s_') : ""
    mask_reads_param = params.mask_short_adapter_reads? "--mask-short-adapter-reads=1" : ""
    no_lane_splitting_param = params.splitlanes_fastq? "" : "--no-lane-splitting"
    create_fastq_for_index_reads_param = params.create_fastq_for_index_reads? "--create-fastq-for-index-reads" : ""
  
    """
    bcl2fastq --runfolder-dir=${runpath} \
    --output-dir=./ \
    ${mask_reads_param} \
    ${tiles_param} \
    ${no_lane_splitting_param} \
    ${create_fastq_for_index_reads_param} \
    --barcode-mismatches=${params.barcode_mismatches} \
    --sample-sheet=${params.samplesheet} \
    --interop-dir=./InterOp/ \
    ${params.extra_parameters}
    """
}

/*
--------------- Create config file for PoolingQC ---------------------
*/
process create_config {
    publishDir "${params.tostoredir}/log", pattern: "config_demux.yml", mode: "copy"
    publishDir "${params.logdir}/${task.process}/${task.hash}", pattern: ".*", mode: "copy"

    input:
    file(demultiplexingstats)

    output:
    file("config_demux.yml")
    path(".*")

    script:
    """
    echo "default:
      directory: ${params.directory}
      demultiplexingstats: ${demultiplexingstats}
      hassampleproject: ${params.hassampleproject}
      lanes: \'${params.lanes}\'
      runname: ${params.runname}
      runpath: ${params.runpath}
      splitlanes_archive: ${params.splitlanes_archive}
      splitlanes_fastq: ${params.splitlanes_fastq}
      splitprojectsarchive: ${params.splitprojectsarchive}" > config_demux.yml    
    """
}

/*
--------------- PoolingQC ---------------------
*/
process poolingQC {
    publishDir "${params.tostoredir}", pattern: "**.{html,png,txt,csv}", mode: "copy"
    publishDir "${params.logdir}/${task.process}/${task.hash}", pattern: ".*", mode: "copy"

    errorStrategy "retry"

    input: 
    file(configfile)
    val(demmuxdone)
    file(demultiplexingstats)

    output: 
    // stdout
    file("**")
    val("poolingqc done"), emit: poolingqc_done
    path(".*")

    script:
    """
    PoolingQC.R ${configfile}
    """
}

/*
---------------Process samplesheet ---------------------
*/
// The sample sheet will be processed to remove all header lines and only keep the table with sample,
// index and project information. This will be needed for wrapping the data and sorting it in the tosend/ output folder
process process_samplesheet {
    publishDir "${params.tostoredir}/log", pattern: "processed_samplesheet.csv", mode: "copy"
    publishDir "${params.logdir}/${task.process}/${task.hash}", pattern: ".*", mode: "copy"

    input:
    file(samplesheet)

    output:
    file("**")
    path(".*")

    script:
    """
    sed -n '/Sample_Name/,\$p' ${samplesheet} > processed_samplesheet.csv
    """
}

/*
--------------- Wrap Demux ---------------------
*/
process wrapdemux {
    publishDir "${params.tosenddir}", pattern: "*.{gz,txt}", saveAs: {filename -> params.splitprojectsarchive? "$project_name/$filename": filename}, mode: "copy"
    publishDir "${params.logdir}/${task.process}/${task.hash}", pattern: ".*", mode: "copy"

    errorStrategy "retry"

    input:
    val(project_name)
    val(demuxdone)

    output:
    file("**")
    tuple val(project_name), file("**")
    // stdout
    path(".*")

    script:
    if(params.hassampleproject && params.splitprojectsarchive)
    """
    tar --use-compress-program="pigz -p ${params.nCPU}" -cf ${params.runname}-RawData-${project_name}.tar.gz -C ${params.outdir}/RawData ${project_name}
    md5sum ${params.runname}-RawData-${project_name}.tar.gz > ${params.runname}-RawData-${project_name}.md5sum.txt
    """
    else if(params.hassampleproject && !params.splitprojectsarchive)
    """
    PROJECTLIST="${project_name}"
    NOCOMMA=\${PROJECTLIST//,/}
    NOFIRSTBRACKET=\${NOCOMMA#[}
    NOLASTBRACKET=\${NOFIRSTBRACKET%]}

    tar --use-compress-program="pigz -p ${params.nCPU}" -cf ${params.runname}-RawData.tar.gz -C ${params.outdir}/RawData \$NOLASTBRACKET
    md5sum ${params.runname}-RawData.tar.gz > ${params.runname}-RawData.md5sum.txt    
    """
    else if(!params.hassampleproject)
    """
    SAMPLELIST="${project_name}"
    NOCOMMA=\${SAMPLELIST//,/}
    NOFIRSTBRACKET=\${NOCOMMA#[}
    NOLASTBRACKET=\${NOFIRSTBRACKET%]}

    SAMPLE_MATCHES=()
    for SAMPLE in \${NOLASTBRACKET[@]}
    do
        SAMPLE_MATCHES+=("\$SAMPLE*")
    done

    tar --use-compress-program="pigz -p ${params.nCPU}" -cf ${params.runname}-RawData.tar.gz -C ${params.outdir}/RawData \$(cd ${params.outdir}/RawData; echo \${SAMPLE_MATCHES[@]})
    md5sum ${params.runname}-RawData.tar.gz > ${params.runname}-RawData.md5sum.txt
    """
}

/*
---------------Copy images ---------------------
*/
process copy_images {
    publishDir "${params.tosenddir}/$project_name", pattern: "*-$project_name-*.pdf", enabled: params.splitprojectsarchive, mode: "copy"
    publishDir "${params.tosenddir}", pattern: "$params.runname-poolingQC*.pdf", enabled: !params.splitprojectsarchive, mode: "copy"
    publishDir "${params.logdir}/${task.process}/${task.hash}", pattern: ".*", mode: "copy"

    input:
    file(imagefile)
    val(project_name)

    output:
    file(imagefile)
    val(project_name)
    tuple val(project_name), path("*${project_name}*.pdf", includeInputs: true) optional true
    path("$params.runname-poolingQC*.pdf", includeInputs: true)
    path(".*")

    script:
    """

    """
}

/*
---------------Add README ---------------------
*/
process add_readme {
    publishDir "${params.tosenddir}",pattern: "README.txt",saveAs: {filename -> params.splitprojectsarchive? "$project_name/$filename": filename}, mode: "copy"
    publishDir "${params.logdir}/${task.process}/${task.hash}", pattern: ".*", mode: "copy"

    input:
    file(readmefile)
    val(project_name)

    output:
    file(readmefile)
    val(project_name)
    tuple val(project_name), path("README.txt", includeInputs: true)
    path("README.txt", includeInputs: true)
    path(".*")

    script:
    """
    """
}

/*
---------------Nextcloud---------------------
*/
process nextcloud {
    publishDir "${params.logdir}/${task.process}/${task.hash}", pattern: ".*", mode: "copy"

    input:
    tuple val(project), file(image1), file(image2), file(md5), file(tar), file(readme), file(netrcfile)

    output:
    stdout
    path(".*")

    script:
    if(params.splitprojectsarchive)
    """
    # create a folder with the project name on the nextcloud server
    curl --netrc-file ${netrcfile} -X MKCOL https://nextnuc.gbiomed.kuleuven.be/remote.php/dav/files/thomas.standaert@kuleuven.be/NUC_NextCloud/Test_Nextflow/${project}

    # upload the files for the project to its folder on the nextcloud server
    GLOBPATTERN="{${image1},${image2},${md5},${tar},${readme}}"
    curl --netrc-file ${netrcfile} -T \${GLOBPATTERN} https://nextnuc.gbiomed.kuleuven.be/remote.php/dav/files/thomas.standaert@kuleuven.be/NUC_NextCloud/Test_Nextflow/${project}/
    """
    else
    """
    # create a folder with the run name on the nextcloud server
    curl --netrc-file ${netrcfile} -X MKCOL https://nextnuc.gbiomed.kuleuven.be/remote.php/dav/files/thomas.standaert@kuleuven.be/NUC_NextCloud/Test_Nextflow/${params.runname}

    # upload the files for the complete run to its folder on the nextcloud server
    GLOBPATTERN="{${image1},${image2},${md5},${tar},${readme}}"
    curl --netrc-file ${netrcfile} -T \${GLOBPATTERN} https://nextnuc.gbiomed.kuleuven.be/remote.php/dav/files/thomas.standaert@kuleuven.be/NUC_NextCloud/Test_Nextflow/${params.runname}/
    """
}

/*
---------------Cellranger Count ---------------------
*/

// The cellranger_count process creates a cellranger count command for all sample files

process cellranger_count {
    publishDir "$params.outdir/Data/cellranger_out", pattern: "${sample}/**", saveAs: {filename -> params.hassampleproject? "$project/$filename" : filename}, mode: "copy"
    publishDir "${params.tostoredir}/log/${task.process}/${task.hash}", pattern: ".*", mode: "copy"
    cpus params.threads

    input:
    tuple val(sample), val(transcriptome), val(project), val(cells)
    val(demuxdone)

    output:
    tuple val(project), val(sample), file("${sample}/outs/metrics_summary.csv"), emit: cellranger_out
    file "${sample}/**"
    path(".*")

    script:
    if( params.hassampleproject )
    """
    cellranger count \
    --id=${sample} \
    --transcriptome=${transcriptome} \
    --fastqs=${params.fastqs_dir}/${project} \
    --sample=${sample} \
    --localcores=${task.cpus} \
    --project=${project} \
    --expect-cells=${cells}
    """
    else if( !params.hassampleproject )
    """
    cellranger count \
    --id=${sample} \
    --transcriptome=${transcriptome} \
    --fastqs=${params.fastqs_dir} \
    --sample=${sample} \
    --localcores=${task.cpus} \
    --expect-cells=${cells}
    """
}

/*
--------------- Generate summary file ---------------------
*/

// The metrics_summary.csv files for each sample, generated by cellranger, will be combined into one summary file
// If the samples have projects, a summary file per project will be generated; otherwise one big summary file is made

// First A file containing the header information is created where later all summary information can be appended to

// extract the line with metrics from each sample; write the sample name and the line to a new file 
process getsummarylines {
    publishDir "${params.logdir}/${task.process}/${task.hash}", pattern: ".*", mode: "copy"

    input:
    tuple val(project), val(sample), file(metrics)

    output:
    tuple val(project), file("**")
    path(".*")

    script:
    """
    echo "${sample},\$(sed -n 2p ${metrics})" > ${sample}_metrics_summary.csv
    """
}

// use the files with the metric lines for each sample to create a summary file per project
process cellranger_summary {
    publishDir "${params.tostoredir}/Robjects/cellranger_summary", pattern: "*.csv", mode: "copy"
    publishDir "${params.logdir}/${task.process}/${task.hash}", pattern: ".*", mode: "copy"

    input:
    tuple val(project), file(summary)

    output:
    tuple val(project), file("*.csv")
    path(".*")

    script:
    if(params.hassampleproject)
    """
    echo "ID,Estimated Number of Cells,Mean Reads per Cell,Median Genes per Cell,Number of Reads,Valid Barcodes,Sequencing Saturation,Q30 Bases in Barcode,Q30 Bases in RNA Read,Q30 Bases in UMI,Reads Mapped to Genome,Reads Mapped Confidently to Genome,Reads Mapped Confidently to Intergenic Regions,Reads Mapped Confidently to Intronic Regions,Reads Mapped Confidently to Exonic Regions,Reads Mapped Confidently to Transcriptome,Reads Mapped Antisense to Gene,Fraction Reads in Cells,Total Genes Detected,Median UMI Counts per Cell
    \$(cat ${summary})" > metrics_summary_${project}.csv
    """
    else if(!params.hassampleproject)
    """
    echo "ID,Estimated Number of Cells,Mean Reads per Cell,Median Genes per Cell,Number of Reads,Valid Barcodes,Sequencing Saturation,Q30 Bases in Barcode,Q30 Bases in RNA Read,Q30 Bases in UMI,Reads Mapped to Genome,Reads Mapped Confidently to Genome,Reads Mapped Confidently to Intergenic Regions,Reads Mapped Confidently to Intronic Regions,Reads Mapped Confidently to Exonic Regions,Reads Mapped Confidently to Transcriptome,Reads Mapped Antisense to Gene,Fraction Reads in Cells,Total Genes Detected,Median UMI Counts per Cell
    \$(cat ${summary})" > metrics_summary_all.csv
    """
}

/*
--------------- Calculate re-pooling ---------------------
*/
process repooling {
    // publishDir "Robjects/repooling", saveAs: {filename -> params.hasproject? "$project/$filename" : filename}, mode: "copy"
    publishDir "${params.tostoredir}/Robjects/repooling", pattern: "**", saveAs: {filename -> params.hassampleproject? "$project-repooling.xlsx" : "re-pooling.xlsx"}, mode: "copy"
    publishDir "${params.logdir}/${task.process}/${task.hash}", pattern: ".*", mode: "copy"


    input:
    tuple val(project), file(summaryfile)

    output:
    file("**")
    path(".*")

    when:
    params.repooling

    script:
    """
    Repooling.R --file "${summaryfile}" --reads "${params.wanted_reads}"
    """
}

/*
--------------- Execute workflow ---------------------
*/

workflow {
    if(params.demultiplexing && params.cellranger) {
        samplesheet = channel.fromPath(params.samplesheet, checkIfExists: true)
        runpath = channel.fromPath(params.runpath, checkIfExists: true)
        noproject = channel.value("Don't split images per project")
        readmefile = channel.fromPath("$projectDir/Nextcloud/README.txt", checkIfExists: true)
        samples = channel.fromPath(params.samplefile) \
        | splitCsv(header:true) \
        | map { row -> tuple(row.Sample_Name, row.Transcriptome, row.Sample_Project, row.Expected_Cells) }
        if(params.nextcloud) { netrcfile = channel.fromPath(params.netrcfile, checkIfExists: true) }

        demultiplexing(samplesheet, runpath)
        create_config(demultiplexing.out.demultiplexingstats)
        poolingQC(create_config.out[0], demultiplexing.out.demultiplexing_out,demultiplexing.out.demultiplexingstats)

        process_samplesheet(samplesheet)
        if(params.hassampleproject && params.splitprojectsarchive) {
            wrapdemux(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique(),poolingQC.out.poolingqc_done.combine(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique()))
            copy_images(poolingQC.out[0].combine(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique()),process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique())
            add_readme(readmefile.combine(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique()),process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique())

            if(params.nextcloud) {
                wrap_split_project = wrapdemux.out[1] | flatten | collate(3)   
                copy_split_project = copy_images.out[2] | flatten | collate(3)
                nextcloud_in_split_project = copy_split_project | join(wrap_split_project) | join(add_readme.out[2]) | combine(netrcfile)
                nextcloud(nextcloud_in_split_project)
            }
            
        }else if(params.hassampleproject && !params.splitprojectsarchive) {
            wrapdemux(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique().toList(),poolingQC.out.poolingqc_done.combine(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique().toList()))
            copy_images(poolingQC.out[0],noproject)
            add_readme(readmefile,noproject)

            if(params.nextcloud) {
                wrap_no_split = wrapdemux.out[0]
                copy_no_split = copy_images.out[3]
                nextcloud_in_no_split = noproject | concat(copy_no_split) | concat(wrap_no_split) | concat(add_readme.out[3]) | concat(netrcfile) | flatten | collate(7)
                nextcloud(nextcloud_in_no_split)
            }
        } else if(!params.hassampleproject) {
            wrapdemux(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Name}.unique().toList(),poolingQC.out.poolingqc_done.combine(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Name}.unique().toList()))
            copy_images(poolingQC.out[0],noproject)
            add_readme(readmefile,noproject)

            if(params.nextcloud) {
                wrap_no_split = wrapdemux.out[0]
                copy_no_split = copy_images.out[3]
                nextcloud_in_no_split = noproject | concat(copy_no_split) | concat(wrap_no_split) | concat(add_readme.out[3]) | concat(netrcfile) | flatten | collate(7)
                nextcloud(nextcloud_in_no_split)
            }
        }  
        cellranger_count(samples, demultiplexing.out.demultiplexing_out.combine(samples))
        getsummarylines(cellranger_count.out.cellranger_out)
        cellranger_summary(getsummarylines.out[0].groupTuple(by:0))
        repooling(cellranger_summary.out[0])
    } else if( params.cellranger) {
        samples = channel.fromPath(params.samplefile) \
        | splitCsv(header:true) \
        | map { row -> tuple(row.Sample_Name, row.Transcriptome, row.Sample_Project, row.Expected_Cells) }
        nodemux = channel.value("no demultiplexing")

        cellranger_count(samples, nodemux)
        getsummarylines(cellranger_count.out.cellranger_out)
        cellranger_summary(getsummarylines.out[0].groupTuple(by:0))
        repooling(cellranger_summary.out[0])
    } else if( params.demultiplexing) {
        samplesheet = channel.fromPath(params.samplesheet, checkIfExists: true)
        runpath = channel.fromPath(params.runpath, checkIfExists: true)
        noproject = channel.value("Don't split images per project")
        readmefile = channel.fromPath("$projectDir/Nextcloud/README.txt", checkIfExists: true)
        if(params.nextcloud) { netrcfile = channel.fromPath(params.netrcfile, checkIfExists: true) }

        demultiplexing(samplesheet, runpath)
        create_config(demultiplexing.out.demultiplexingstats)
        poolingQC(create_config.out[0], demultiplexing.out.demultiplexing_out, demultiplexing.out.demultiplexingstats)
        process_samplesheet(samplesheet)
        if(params.hassampleproject && params.splitprojectsarchive) {
            wrapdemux(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique(),poolingQC.out.poolingqc_done.combine(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique()))
            copy_images(poolingQC.out[0].combine(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique()),process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique())
            add_readme(readmefile.combine(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique()),process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique())

            if(params.nextcloud) {
                wrap_split_project = wrapdemux.out[1] | flatten | collate(3) 
                copy_split_project = copy_images.out[2] | flatten | collate(3)
                nextcloud_in_split_project = copy_split_project | join(wrap_split_project) | join(add_readme.out[2]) | combine(netrcfile)
                nextcloud(nextcloud_in_split_project)
            }
        }else if(params.hassampleproject && !params.splitprojectsarchive) {
            wrapdemux(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique().toList(),poolingQC.out.poolingqc_done.combine(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Project}.unique().toList()))
            copy_images(poolingQC.out[0],noproject)
            add_readme(readmefile,noproject)

            if(params.nextcloud) {              
                wrap_no_split = wrapdemux.out[0]
                copy_no_split = copy_images.out[3]
                nextcloud_in_no_split = noproject | concat(copy_no_split) | concat(wrap_no_split) | concat(add_readme.out[3]) | concat(netrcfile) | flatten | collate(7)
                nextcloud(nextcloud_in_no_split)
            }
        } else if(!params.hassampleproject) {
            wrapdemux(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Name}.unique().toList(),poolingQC.out.poolingqc_done.combine(process_samplesheet.out[0].splitCsv(header:true).map {row -> row.Sample_Name}.unique().toList()))
            copy_images(poolingQC.out[0],noproject)
            add_readme(readmefile,noproject)

            if(params.nextcloud) {
                wrap_no_split = wrapdemux.out[0]
                copy_no_split = copy_images.out[3]
                nextcloud_in_no_split = noproject | concat(copy_no_split) | concat(wrap_no_split) | concat(add_readme.out[3]) | concat(netrcfile) | flatten | collate(7)
                nextcloud(nextcloud_in_no_split)
            }
        }  
    }
}
