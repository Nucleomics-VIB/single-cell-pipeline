#!/bin/bash

# Script for creating the a directory structure on madiba under /DATA/RunManagement based on runname
# The script should also create following directories: Config (config file, samplesheet), Scripts(git pull single-cell-pipeline), ToStore

###############################################################################
# Functionality
###############################################################################
# 0. Get current working directory
# 1.take arguments from GP (runname, arguments for creating the config file for nextflow script)
# 2. create directory based on runname in right place (first check if the directory already exists?)
# 3. create subdirectories
# 4. pull the single-cell-pipeline git repository



## 0. Get current working directory ##
STARTDIR=$(pwd)


## 1.take arguments from GP (for now only runname) ##
RUNPATH="$1"

## 1.1 Extract the run name out of the full path
RUNNAME="$(basename "$RUNPATH")"

## 2. create directory based on runname in right place (first check if the directory already exists?) ##
RUNMANAGEMENTDIR="/DATA/RunManagement"
RUNSDIR="$RUNMANAGEMENTDIR/$RUNNAME"
SCRIPTSDIR="$RUNSDIR/Scripts"
CONFIGDIR="$RUNSDIR/Config"
TOSTOREDIR="$RUNSDIR/ToStore"

# for testing
# RUNMANAGEMENTDIR="/home/kobe/testing/bash"
# RUNSDIR="$RUNMANAGEMENTDIR/$RUNNAME"
# SCRIPTSDIR="$RUNSDIR/Scripts"
# CONFIGDIR="$RUNSDIR/Config"
# TOSTOREDIR="$RUNSDIR/ToStore"


# check if the dir already exists
if [ -d "$RUNSDIR" ]
then
  echo "There already exists a directory with the name $RUNNAME"
  exit 1
fi

mkdir "$RUNSDIR"
mkdir "$SCRIPTSDIR"
mkdir "$CONFIGDIR"
mkdir "$TOSTOREDIR"

# # move to the runsdir
# cd "$RUNSDIR"

## 4. pull the single-cell-pipeline git repository ##
GITREPO="git@gitlab.com:Nucleomics-VIB/single-cell-pipeline.git"
# cd "$SCRIPTSDIR"
git clone "$GITREPO" "$SCRIPTSDIR/single-cell-pipeline"