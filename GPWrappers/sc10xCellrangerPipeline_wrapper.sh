#!/bin/bash

# Wrapper script for the TenxCellrangerPipeline module


###############################################################
# Wrapper script
###############################################################
# 1. get the current working directory
startdir=$(pwd)

# 2. read the labelfile (first passed argument) and define the rundirectory from this info
labelfile="$1"
workdir="/DATA/Projects/$(cat "$labelfile")"


# 3. change to the working directory
cd "$workdir"
# echo "$workdir"

# 3.1 Need to remove the first line of the yaml file for nextflow to recognize the params
configfile="$3"
/usr/bin/sed 1d "$configfile" > config_nextflow.yml

# 5. run the nextflow script and pass the rest of the arguments to it
# /opt/tools/nextflow run Scripts/single-cell-pipeline/Scripts/TenxCellrangerPipeline.nf "${@:2}"
/opt/tools/nextflow run Scripts/single-cell-pipeline/Scripts/TenxCellrangerPipeline.nf "-params-file" "config_nextflow.yml" "${@:4}"

# test:
# echo "${@:2}" > Kobe_logfile.txt

# 6. link back the interesting files to the starting directory
# ln -s "$workdir"/cellranger_out/summary/* "$startdir"
ln -s "$workdir"/Robjects/** "$startdir"
ln -s "$workdir"/Images/*** "$startdir"
ln -s "$workdir"/report.html "$startdir"
ln -s "$workdir"/timeline.html "$startdir"
ln -s "$workdir"/*LogFile.html "$startdir"

