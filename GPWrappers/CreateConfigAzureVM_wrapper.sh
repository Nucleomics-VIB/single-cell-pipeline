#!/bin/bash

# Script for creating a config file for the scripts in the single-cell-pipeline repository that manage the Azure VM


###############################################################################
# Functionality
###############################################################################

# 1. Get current working directory
# 2. take arguments from GP (runpath, arguments for creating the config file)
# 3. Check if rundir exists
# 4. Change to the configdir
# 5. Run the CreateConfigAzureVM.R script for creating the config file and send all the arguments to it
# 6. Link back the output files to the GP job directory



###############################################################################
# Script
###############################################################################
# 1. Get current working directory
STARTDIR=$(pwd)

# 2. take arguments from GP (rundir, arguments for creating the config file)
RUNPATH="$2"

# extract run name out of the runpath path
RUNNAME="$(basename "${RUNPATH}")"

# extract rundir path out of runname
RUNDIR="/DATA/RunManagement/${RUNNAME}"
CONFIGDIR="$RUNDIR/Config"
SCRIPTSDIR="$RUNDIR/Scripts"

# 3. Check if rundir exists
if [ ! -d "$RUNDIR" ]
then
  echo "There is no directory the name $RUNDIR, first create it using the StartNewRun module in GenePattern"
  exit 1
fi

# check if the configdir exists
if [ ! -d "$CONFIGDIR" ]
then
  echo "There is no directory the name $CONFIGDIR, first create it using the StartNewRun module in GenePattern"
  exit 1
fi

# check if the scriptsdir exists
if [ ! -d "$SCRIPTSDIR" ]
then
  echo "There is no directory the name $SCRIPTSDIR, first create it using the StartNewRun module in GenePattern"
  exit 1
fi

# 4. Change to the configdir
cd "$CONFIGDIR"

# 5. Run the CreateConfigAzureVM.R script for creating the config file and send all the arguments to it
# activate the conda environment
source /opt/tools/miniconda3/etc/profile.d/conda.sh
conda activate scTenxDemux

# run the script and pass the arguments
Rscript "$SCRIPTSDIR/single-cell-pipeline/Scripts/CreateConfigAzureVM.R" "${@}"

# 6. Link back the output files to the GP job directory
ln -s "$CONFIGDIR/config.yaml" "$STARTDIR"

# 7. Run the SearchAndTransferClient.sh script witht he config file as argument
# first change back to GP dir
cd "${STARTDIR}"

# run the script
. "${SCRIPTSDIR}"/single-cell-pipeline/Scripts/Bash/SearchAndTransferServer.sh "${CONFIGDIR}"/config.yaml CopyComplete.txt
