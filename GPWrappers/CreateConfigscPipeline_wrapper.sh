#!/bin/bash

# Script for creating a config file for TenxCellrangerPipeline.nf script

###############################################################################
# DEFINE FUNCTIONS
###############################################################################


## Functionality ##
###################

# 1. Get current working directory
# 2.take arguments from GP (runmanagementdir, arguments for creating the config file for nextflow script)
# 3. Change to the configdir
# 4. Run the CreateConfigscPipeline.R script for creating the config file and send all the arguments to it
# 5. Link back the output files to the GP job directory



## 1. Get current working directory ##
STARTDIR=$(pwd)


## 2.take arguments from GP (rundir, arguments for creating the config file for nextflow script) ##
# The second argument needs to be the runname (first will be the flag for runname)-> will be used for both creating the directory and for sending to the R script
RUNDIR="$2"
echo "$RUNDIR"

CONFIGDIR="$RUNDIR/Config"
SCRIPTSDIR="$RUNDIR/Scripts"


# 3. Change to the configdir

# check if the rundir exists
if [ ! -d "$RUNDIR" ]
then
  echo "There is no directory the name $RUNDIR, first create it using the StartNewRun module in GenePattern"
  exit 1
fi

# check if the configdir exists
if [ ! -d "$CONFIGDIR" ]
then
  echo "There is no directory the name $CONFIGDIR, first create it using the StartNewRun module in GenePattern"
  exit 1
fi

# check if the scriptsdir exists
if [ ! -d "$SCRIPTSDIR" ]
then
  echo "There is no directory the name $SCRIPTSDIR, first create it using the StartNewRun module in GenePattern"
  exit 1
fi

## 3. Change to the configdir ##
cd "$CONFIGDIR"


## 4. Run the CreateConfigscPipeline.R script for creating the config file and send all the arguments to it ##
# activate the conda environment
source /opt/tools/miniconda3/etc/profile.d/conda.sh
conda activate scTenxDemux

# run the script and pass the arguments
Rscript "$SCRIPTSDIR/single-cell-pipeline/Scripts/CreateConfigscPipeline.R" "${@}"

# 6. Link back the output files to the GP job directory

ln -s "$CONFIGDIR"/* "$STARTDIR"
