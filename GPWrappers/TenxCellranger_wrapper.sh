#!/bin/bash

# Wrapper script for the TenxCellranger module

###############################################################
# steps for the wrapper script:
###############################################################

# 1. get the current working directory
# 2. read the labelfile (first passed argument) and define the rundirectory from this info
# 3. change to the working directory
# (4. create a log file (maybe make separate directory for it?))
# 5. run the nextflow script and pass the rest of the arguments to it
# 6. link back the interesting files to the starting directory

# arguments to pass to the wrapper script (extra things to do)
# 1. Labelfile for extracting the rundirectory -> take this as first overall argument
# (2. if they want the matrices and individual summary files in the GP output?)

# arguments to pass to the cellranger nextflow script:
# 1. samplefile
# 2. fastqs_dir
# 3. threads
# 4. hasproject

# put these arguments after the mandatory arguments -> this way we can specify to pass the
# arguments to the nextflow script, starting from argument x later

###############################################################
# Wrapper script
###############################################################
# 1. get the current working directory
startdir=$(pwd)

# 2. read the labelfile (first passed argument) and define the rundirectory from this info
labelfile="$1"
workdir="/DATA/Projects/$(cat "$labelfile")"

# 3. change to the working directory
cd "$workdir"
# echo "$workdir"

# 5. run the nextflow script and pass the rest of the arguments to it
/opt/tools/nextflow run Scripts/single-cell-pipeline/Scripts/CellrangerCount.nf "${@:2}"
# test:
# echo "${@:2}" > Kobe_logfile.txt

# 6. link back the interesting files to the starting directory
# ln -s "$workdir"/cellranger_out/summary/* "$startdir"
ln -s "$workdir"/Robjects/cellranger_summary/** "$startdir"
ln -s "$workdir"/Robjects/repooling/** "$startdir"


# test:
# ln -s "$workdir"/Kobe_logfile.txt "$startdir"

# possibly add another parameter to the genepattern module to specify what is wanted as output
# possible to link all individual metrics_summary.csv files for each sample 
# also possible to link the feature and gene matrices for each sample

# link the cellranger_out directory to the original directory where GenePattern ran the module from
# ln -s $targetdir/cellranger_out/* $currdirr
# this would link all the files in the directory, this is way to much output for GenePattern
