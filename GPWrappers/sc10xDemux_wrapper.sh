#!/bin/bash

# wrapper script for the sc10xDemux module. The script activates the conda
# environment "scTenxDemux" and executes the "sc10xDemux_wrapper.R" script

# Activate the conda environment
source /opt/tools/miniconda3/etc/profile.d/conda.sh
conda activate scTenxDemux

# Run the wrapper script and pass the configfile argument
Rscript $1 $2