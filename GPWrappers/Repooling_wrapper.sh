#!/bin/bash

# Wrapper script for the Repooling module

###############################################################
# steps for the wrapper script:
###############################################################
# 1. Activate the conda environment
# 2. Get the current working directory
# 3. Read the labelfile (first passed argument) and define the rundirectory from this info
# 4. Change to the working directory
# 5. Run the Repooling.R script
# 6. Link back the re-pooling.xlsx file

###############################################################
# Wrapper script
###############################################################

# 1. Activate the conda environment
source /opt/tools/miniconda3/etc/profile.d/conda.sh
conda activate scTenxDemux

# 2. Get the current working directory
startdir=$(pwd)

# 3. Read the labelfile (first passed argument) and define the rundirectory from this info
labelfile="$1"
workdir="/DATA/Projects/$(cat "$labelfile")"

# 4. Change to the working directory
cd "$workdir"

# 5. Run the Repooling.R script
Rscript Scripts/single-cell-pipeline/Scripts/Repooling.R "${@:2}" 

# 6. Link back the re-pooling.xlsx file
ln -s "$workdir"/Robjects/re-pooling.xlsx "$startdir"

