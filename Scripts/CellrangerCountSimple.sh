
# Bash script that constructs a cellranger count command to get the gene matriix for a fastq sample

###############################################################################
VALIDATE INPUT
###############################################################################
function usage() {
    errorString="usage: $0\n
    Running cellrangercountsimple requires 4 arguments:\n
    1. Path to the project folder containing sample folders with fastq files\n
    2. The sample ID of the sample than mapped. It's the \n
    3. The path to the transcriptome index\n
    4. The number of processer cores to be used by the system"
    echo -e $errorString
    exit 1
}

# validate number of arguments
if [ $# -ne 4 ]
then
	usage
fi

# assign arguments to variable names
FASTQPATH=$1
SAMPLEID=$2
TRANSCRIPTOME=$3
THREADS=$4


# how the command should look like
# /opt/tools/cellranger-3.1.0/cellranger count \
# --id=T1 \
# --fastqs=/data/Projects/3713_FSvedberg_NovaSeq/RawData/3745 \
# --sample=T1_10X_260321 \
# --transcriptome=/data/ResourceData/CellRanger/refdata-cellranger-mm10-1.2.0/ \
# --localcores=12

# create the command
command="cellranger count"
command="$command --id=$SAMPLEID"
command="$command --transcriptome=$TRANSCRIPTOME"
command="$command --fastqs=$FASTQPATH"
command="$command --sample=$SAMPLEID"
command="$command --localcores=$THREADS"

# run the command
START_TIME=$(date)
eval $command
END_TIME=$(date)
echo "Run Time: $START_TIME -> $END_TIME"

