$archivedir = '/mnt/nuc-data/GBW-0011_NUC/Projects';
$workingdir = '/data/Projects';

use Getopt::Long;

GetOptions(\%options,
  "labelfile=s",
  "forcecopy=s", # yes or no (the default)
  "forceclone=s" # yes or no (the default)
);

# read label file to find in which project we are working
# and which script is needed
open LABEL, $options{labelfile};
$projectName = <LABEL>;
chomp $projectName;
close LABEL;
if (not -e "$archivedir/$projectName" ) {
  die "cannot find folder $archivedir/$projectName\n";
}
$projectName =~ /^(\d+)_(.+)_(.+)$/;
$ExpNr = $1 ; $ExpName = $2 ; $platform = $3;

# check that project does not exists yet in working area or alternatively
# that it exists but is not being worked on and that overwriting is OK.
# Then set flag
$flag = "$workingdir/$projectName/GP_ANALYSIS_RUNNING";
if (-e "$workingdir/$projectName") {
  if ($options{forcecopy} eq 'no') {
    die "Cannot create project folder in working folder because it exists already.\nSet parameter \"forcecopy\" to \"yes\" to overwrite.";
  } else {
    if (-e $flag) {
      die "Cannot copy data into this project,\n\"flag\" GP_ANALYSIS_RUNNING set !\n";
    }
  }
} else {
  mkdir "$workingdir/$projectName";
}
open FLAG, ">$flag";
  print FLAG "This file is a \"flag\" indicating that an analysis is already running in\nthis project (or that an analysis crashed). Remove it at your own peril.\n";
close FLAG;

# copy all data and eventually overwrite, but do not delete files
# (we do not copy data that are not usefull in working folder)
system "/usr/bin/rsync -av --exclude='WetLab' --exclude='Workflow_Exp*' --exclude='ToSend' $archivedir/$projectName $workingdir";

# for Illumina data get scripts from GIT
# for other data just put README file with instructions for how to
#   retrieve data from GIT
if (not -e "$workingdir/$projectName/Scripts") {
  mkdir "$workingdir/$projectName/Scripts";
}
if ($platform =~ /(Mi|Hi|Next|Nova)Seq/) {
  if (-e "$workingdir/$projectName/Scripts/MassiveParallelSequencingAnalysis") {
    if ($options{forceclone} eq 'yes') {
      system "/bin/rm -fr $workingdir/$projectName/Scripts/MassiveParallelSequencingAnalysis";
      &CloneFromGIT;
    } else {
      print "Not retrieving scripts MassiveParallelSequencingAnalysis from GIT,\nsince they are already there. Set parameter \"forceclone\" to \"yes\"\nto force replacing existing scripts by recent scripts from GIT.";
    }
  } else {
    &CloneFromGIT;
  }
  if (-e "$workingdir/$projectName/Scripts/single-cell-pipeline") {
    if ($options{forceclone} eq 'yes') {
      system "/bin/rm -fr $workingdir/$projectName/Scripts/single-cell-pipeline";
      &CloneFromGITscWorkflow;
    } else {
      print "Not retrieving scripts single-cell-pipeline from GIT,\nsince they are already there. Set parameter \"forceclone\" to \"yes\"\nto force replacing existing scripts by recent scripts from GIT.";
    }
  } else {
    &CloneFromGITscWorkflow;
  }
} else { # leave it to the user what to do
  open README, ">$workingdir/$projectName/Scripts/README_aboutGIT";
  $message = <<END_MESSAGE;
You can retrieve scripts from GitLab by logging in Madiba and
doing in this directory one of the following commands :

sudo -u genepattern git clone git\@gitlab.com:Nucleomics-VIB-private/MassiveParallelSequencingAnalysis.git
sudo -u genepattern git clone git\@gitlab.com:Nucleomics-VIB-private/nuc-tools.git
sudo -u genepattern git clone git\@gitlab.com:Nucleomics-VIB/bionano-tools.git
sudo -u genepattern git clone git\@gitlab.com:Nucleomics-VIB/single-cell-pipeline.git
END_MESSAGE
  print README $message;
  close README;
}

# remove flag
unlink $flag;

sub CloneFromGIT {
  system "cd \"$workingdir/$projectName/Scripts\" ; git clone git\@gitlab.com:Nucleomics-VIB-private/MassiveParallelSequencingAnalysis.git";
}

sub CloneFromGITscWorkflow {
  system "cd \"$workingdir/$projectName/Scripts\" ; git clone git\@gitlab.com:Nucleomics-VIB/single-cell-pipeline.git";
}
