$projectdir = '/mnt/nuc-data/GBW-0011_NUC/Projects';
$workflowtemplate = '/mnt/nuc-data/GBW-0011_NUC/AppData/CreateProjectDirectory/Copy-to-Y-projects/Workflow_Exp0000.docm';

$ExpNr = $ARGV[0]; $ExpName = $ARGV[1]; $platform = $ARGV[2];
$projectName = $ExpNr . '_' . $ExpName . '_' .  $platform;

# test if location Project dir on machine is still the same
# and that Project has not already been created
opendir PROJECTDIR, $projectdir or die "cannot open $projectdir\n";
closedir PROJECTDIR;
mkdir "$projectdir/$projectName" or die "$projectName exists already !\n";

# create label file for later easy pointing to Project in GenePattern
open LABEL, ">$projectName.label";
print LABEL $projectName;
close LABEL;

# create directories for project
if ($platform eq 'UnknownExperiment') {  die "cannot do anything for unknown platform!\n"}
if ($platform ne 'BioInfoOnly' && $platform ne 'SCWorkflow') {
 mkdir "$projectdir/$projectName/WetLab";
}
if ($platform ne 'SampleQC') {
  mkdir "$projectdir/$projectName/Data";
  mkdir "$projectdir/$projectName/RawData";
  mkdir "$projectdir/$projectName/Robjects";
  mkdir "$projectdir/$projectName/Images";
  mkdir "$projectdir/$projectName/Report";
  mkdir "$projectdir/$projectName/ToSend";
}
mkdir "$projectdir/$projectName/Scripts";

# copy workflow template
if ($platform ne 'BioInfoOnly' && $platform ne 'SCWorkflow') {
  if (-e $workflowtemplate) {
    system "/bin/cp $workflowtemplate $projectdir/$projectName/Workflow_Exp${ExpNr}.docm";
  } else {
  warn "cannot find $workflowtemplate\n";
  }
}
