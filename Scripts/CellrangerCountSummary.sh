# Script for combining all metrics.summary.csv files of a specific
# project. This file was created to be used after CellrangerCount.sh
# in the same working directory and witht he same sample sheet provided

################################################################################
# DEFINE FUNCTIONS
################################################################################

# usage message that explains the arguments when the wrong number of arguments
# is provided
function usage() {
    errorString="usage: $0\n
    Running CellrangerCountSummary requires 2 arguments:\n
    1. The name of the project that will be summarised, as specified in the \n
    Sample_Project column of the samplesheet\n
    2. The path tot he sample sheet"
    echo -e $errorString
    exit 1
}

################################################################################
# VALIDATE INPUT
################################################################################
# validate the number of arguments
if [ $# -ne 2 ]
then
	usage
fi

# assign arguments to variable names
PROJECT=$1
SAMPLESHEET=$2

###############################################################################
# EXTRACT SAMPLE NAMES AND DIRECTORIES OUT OF SAMPLE SHEET
###############################################################################
SAMPLES=$(sed "1,/Sample_Name/d" $SAMPLESHEET | grep $PROJECT | cut -f 3 -d "," | sort -u)

# Store the samples in an array
ARR=($SAMPLES)

# create an array for storing the directory names containing the summary files
SUMDIRS=()
for SAMPLE in "${ARR[@]}"
do
    SUMDIRS+=("$SAMPLE/outs/metrics_summary.csv")
done

###############################################################################
# CREATE SUMMARY TABLE AND WRITE TO A FILE
###############################################################################
# Define the file where the summary table will be written to
OUTFILE="metrics_summary_$PROJECT.csv"

# Create a temporary file for storing the temporary summary table
TEMPFILE="temp_metrics_summary_$PROJECT.csv"

# Combine the first 2 lines of the first summary file witht he second line of
# all other summary files to get a table with a header and all the data
awk '(NR == 1) || (FNR > 1)' "${SUMDIRS[@]}" > "$TEMPFILE"

# Add a column containing the sample names at the beginning of the table and
# write the final summary table to the outfile
printf "%s\n" "ID" "${ARR[@]}" | paste -d, - "$TEMPFILE" > "$OUTFILE"

# remove the temporary file
rm "$TEMPFILE"