# Script for executing cellranger count for multiple samples

###############################################################################
# DEFINE FUNCTIONS
###############################################################################

# usage message that explains the arguments when the wrong number of arguments
# is provided
function usage() {
    errorString="usage: $0\n
    Running cellrangerCount.sh requires 5 arguments:\n
    1. 'all' if you want to map all projects or the project name from the \n
    samplesheet if you want to only map the samples from one project\n
    2. Path to the project folder containing sample folders with fastq files\n
    3. The path to the sample sheet \n
    4. The path to the transcriptome index\n
    5. The number of processer cores to be used by the system"
    echo -e $errorString
    exit 1
}

# construct and run the cellranger count script for one sample at a time
function cellranger_counting() {
    local SAMPLE_ID=$1
    command="/opt/tools/cellranger-3.1.0/cellranger count"
    command="$command --id=$SAMPLE_ID"
    command="$command --transcriptome=$TRANSCRIPTOME"
    command="$command --fastqs=$FASTQPATH"
    command="$command --sample=$SAMPLE_ID"
    command="$command --localcores=$THREADS"

    local START_TIME=$(date)
    eval $command
    # echo $command
    local END_TIME=$(date)
    echo "Run Time: $START_TIME -> $END_TIME"
}

###############################################################################
# VALIDATE INPUT
###############################################################################
# validate number of arguments
if [ $# -ne 5 ]
then
	usage
fi

# assign arguments to variable names
PROJECT=$1
FASTQPATH=$2
CSV=$3
TRANSCRIPTOME=$4
THREADS=$5

###############################################################################
# EXTRACT SAMPLE NAMES OUT OF SAMPLE SHEAT
###############################################################################
# if a projectname is given, only include samples from that project name, else
# include all teh samples
if [ $PROJECT == "all" ]
then 
    echo "Project should be all. \$PROJECT = $PROJECT"
    SAMPLES=$(sed "1,/Sample_Name/d" $CSV | cut -f 3 -d "," | sort -u | xargs)
else
    SAMPLES=$(sed "1,/Sample_Name/d" $CSV | grep $PROJECT | cut -f 3 -d "," | sort -u | xargs)
fi

# echo $SAMPLES

###############################################################################
# Run the command
###############################################################################
for SAMPLE in $SAMPLES
do
    cellranger_counting $SAMPLE
done