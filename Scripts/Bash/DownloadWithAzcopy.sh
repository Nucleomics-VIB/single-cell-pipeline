#! /usr/bin/env bash
# 
# Author: Thomas Standaert <thomas.standaert@vib.be>
# Version: 1.0.0 - 16/12/2021

# {{{ Bash settings
# abort on nonzero exitstatus
set -o errexit
# abort on unbound variable
set -o nounset
# don't hide errors within pipes
set -o pipefail
#}}}

# {{{ Variables
readonly config_file=$1
readonly nextcloud_folder="$(yq eval '.mountpoint.nextcloud_path' \
    ${config_file})"
readonly store_location="$(yq eval '.mountpoint.madiba_data' \
    ${config_file})"
readonly blob_output="$(yq eval '.azure.blobstorage' ${config_file})"
readonly sas_token="$(yq eval '.azure.sastoken' ${config_file})"
readonly flag="$(yq eval '.flags.remote_local_complete' \
    ${config_file})"
readonly run_name="$(yq eval '.run.run_name' ${config_file})"
# }}}

check_mount(){
    local mount_point=$1
    mountpoint -q "${mount_point}" \
        && echo "Mount ${mount_point} is good." \
        || ( echo "${mount_point} is not loaded. Exiting." && exit 1 )
}

download_folder(){
    # Download the whole blob storage as one
    while true; do
        check_flag="azcopy list '${blob_output}/${sas_token}' \
| grep ${flag}"
        echo $check_flag
        if eval ${check_flag}; then
            download_ToSend="azcopy copy '${blob_output}/ToSend/*${sas_token}' \
'${nextcloud_folder}' --recursive"
            download_ToStore="azcopy copy '${blob_output}/ToStore/*${sas_token}' \
'${store_location}/${run_name}/ToStore' --recursive"
            echo $download_ToSend
            echo $download_ToStore
            eval "${download_ToStore}"
            eval "${download_ToSend}"
            echo "Finished downloading"
            remove_flag="azcopy rm '${blob_output}/${flag}\
/${sas_token}' --recursive"
            echo "${remove_flag}"
            eval "${remove_flag}"
            break
        fi
        echo "Sleeping..."
        sleep 1h
    done
}

main(){
    mount_point=$(echo "${nextcloud_folder}" | cut -f1-3 -d '/')
    check_mount "${mount_point}"
    download_folder    
}

main
