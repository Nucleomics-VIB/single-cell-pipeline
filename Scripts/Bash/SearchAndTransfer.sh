#! /usr/bin/env bash
# 
# Author: Thomas Standaert <thomas.standaert@vib.be>
# Version: 1.0.0 - 12/10/2021
# Parent script to find and copy rundata, based on a text flag.
# The script uses yq to read a yaml config file. Be sure to install it 
# and put it in your PATH. More explanation can be found here:
# https://github.com/mikefarah/yq 

# {{{ Bash settings
# abort on nonzero exitstatus
set -o errexit
# abort on unbound variable
set -o nounset
# don't hide errors within pipes
set -o pipefail
#}}}
# {{{ Variables
readonly config_file=$1
readonly run_name="$(yq eval '.run.run_name' ${config_file})"
readonly run_path="$(yq eval '.run.run_path' ${config_file})"
readonly sequencer=''
# Azure bound variables
readonly sas_token="$(yq eval '.azure.sastoken' ${config_file})"
readonly azure_input="$(yq eval '.azure.input' ${config_file})"
readonly azure_output="$(yq eval '.azure.output' ${config_file})"
readonly blob_output="$(yq eval '.azure.blobstorage' ${config_file})"
readonly input_mount="$(yq eval '.mountpoint.azure_input' ${config_file})"
#}}}

# Check if mount is correctly mounted
pa_check_mount(){
    local mount_point=$1
    mountpoint -q "${mount_point}" \
        && echo "Mount ${mount_point} is good." \
        || ( echo "${mount_point} is not loaded. Exiting." && exit 1 )
}


# Check if file is presents
pa_check_directory(){
    local directory=$1
    [ -d "${directory}" ] && echo "${directory} is present." \
        || ( echo "${directory} is not present. Exiting." && exit 1 )
}

pa_check_file_is_finished(){
    local file_flag=$1
    local location=$2
    case "${location}" in
        local_to_remote)
        local mount_point="${run_path}"
        ;;
        remote)
        local mount_point="${input_mount}/${run_name}"
        ;;
    esac
    [[ -n $(find "${mount_point}/${run_name}" -maxdepth 1 -name "${file_flag}") ]] && return
}



