#!/usr/bin/env bash
# 
# Author: Thomas Standaert <thomas.standaert@vib.be>
# Version: 1.0.0 - 08/11/2021
# Script that will check if a new directory appears in the input file.
# This works by putting the output of find in an array. This array 
# will then be parsed and SearchAndTransferClient.sh will be run,
# as we know were the config file is found (standard structure).

# {{{ Bash settings
# abort on nonzero exitstatus
set -o errexit
# abort on unbound variable
set -o nounset
# don't hide errors within pipes
set -o pipefail
#}}}

# {{{ Variables
readonly input_path='/mnt/input'
readonly cron_time='15'
readonly config_file_rpath='Config/config.yaml'
readonly scripts_rpath='Scripts/single-cell-pipeline/Scripts/Bash'
#}}}

# Make an array with the output from find. max and mindepth are to keep
# the results on the level just under parent while excluding that one.
# Cmin is fixed on the cron time, to get the ones created between the
# two cron runs.
#set -u
#declare -a new_directories=()
#while IFS=  read -r -d $'\0'
#do
#    new_directories+=("$REPLY")
#done < <(find ~ -maxdepth 1 -mindepth 1 -type d -cmin -${cron_time} -print0)
echo "Starting script"
readarray -t new_directories < <(find ${input_path} -maxdepth 1 -mindepth 1 -type d -cmin -${cron_time})
for new_directory in "${new_directories[@]}"
do
    #cmd="cd v${new_directory}/${scripts_rpath}"
    #echo "${cmd}"
    #eval "${cmd}"
    cmd="${new_directory}/${scripts_rpath}/SearchAndTransferClient.sh ${new_directory}/${config_file_rpath}"
    echo "${cmd}"
    eval "${cmd}"
done
