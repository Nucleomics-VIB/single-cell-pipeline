#! /usr/bin/env bash
# 
# Author: Thomas Standaert <thomas.standaert@vib.be>
# Version: 1.0.0 - 14/10/2021
# Parent script to find and copy rundata, based on a text flag.
# The script uses yq to read a yaml config file. Be sure to install it 
# and put it in your PATH. More explanation can be found here:
# https://github.com/mikefarah/yq 

# {{{ Bash settings
# abort on nonzero exitstatus
set -o errexit
# abort on unbound variable
set -o nounset
# don't hide errors within pipes
set -o pipefail
#}}}

# {{{ Include the parent script
run_path=$(dirname $(dirname $1))
source "${run_path}/Scripts/single-cell-pipeline/Scripts/Bash/SearchAndTransfer.sh" $1
# }}}

# {{{ Variables
# Flags
echo config file in client is ${config_file}
readonly init_copy_flag="$(yq eval '.flags.local_remote_complete' \
${config_file})"
readonly nf_finished_flag="$(yq eval '.flags.remote_local_complete' \
${config_file})"
readonly copy_location='remote'
# Conda variables
readonly conda_sh="$(yq eval '.conda.conda_sh' ${config_file})"
readonly env_name="$(yq eval '.conda.env_name' ${config_file})"
# NextFlow variables
readonly nf_script_path="$(yq eval '.nextflow.script_path' \
${config_file})"
readonly nf_script_name="$(yq eval '.nextflow.script_name' \
${config_file})"
readonly nf_config_file="$(yq eval '.nextflow.config_file' \
${config_file})"
readonly nf_config_path="$(yq eval '.nextflow.config_path' \
${config_file})"

#}}}

start_conda_env(){
    cmd=". ${conda_sh}"
    echo $cmd 
    eval "${cmd}"
    cmd="conda activate ${env_name}"
    echo $cmd
    set +u
    eval "${cmd}"
    set -u
}

run_nextflow(){
    cmd="nextflow run ${input_mount}/${nf_script_path}/${nf_script_name}\
 -params-file ${input_mount}/${nf_config_path}/${nf_config_file} \
 -profile az_test"  
    echo $cmd
    eval "${cmd}"
}

upload_flag(){
    touch "${nf_finished_flag}"
    azcopy_cmd="azcopy copy ${nf_finished_flag} '${blob_output}${sas_token}'"
    echo ${azcopy_cmd}
    eval ${azcopy_cmd}
    rm "${nf_finished_flag}"
}

main(){
    pa_check_mount "${input_mount}"
    #pa_check_mount "${output_mount}"
    while true
    do
        if pa_check_file_is_finished "${init_copy_flag}" "${copy_location}"
        then
            start_conda_env 
            run_nextflow
            upload_flag
            return
        else
            echo "still waiting"
            sleep 15m
        fi
    done
}

main
