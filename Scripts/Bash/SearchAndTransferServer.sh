#! /usr/bin/env bash
# 
# Author: Thomas Standaert <thomas.standaert@vib.be>
# Version: 1.0.0 - 13/10/2021
# Child script from SearchAbndTransfer.sh script to find and 
# copy rundata, based on a text flag, for use on our local server.
# The script uses yq to read a yaml config file. Be sure to install it 
# and put it in your PATH. More explanation can be found here:
# https://github.com/mikefarah/yq 

# {{{ Bash settings
# abort on nonzero exitstatus
set -o errexit
# abort on unbound variable
set -o nounset
# don't hide errors within pipes
set -o pipefail
#}}}

# {{{ Include the parent script
run_path=$(dirname $(dirname $1))
source "${run_path}/Scripts/single-cell-pipeline/Scripts/Bash/SearchAndTransfer.sh" $1
# }}}

# {{{ Variables
readonly copy_flag=$2
readonly copy_location='local_to_remote'
readonly run_mount="$(yq eval '.mountpoint.run_mnt' ${config_file})"
readonly finish_copy_flag="$(yq eval '.flags.local_remote_complete' \
     ${config_file})"
readonly run_config="$(yq eval '.run.run_config' ${config_file})"

#}}}

copy_data_to_azure(){
    # Copy run config
    cmd="azcopy copy \"${run_config}\" \"${azure_input}${sas_token}\" \
 --recursive"
    echo ${cmd}
    eval "${cmd}"
    # Copy run data
    cmd="azcopy copy \"${run_path}/${run_name}\" \"${azure_input}\
/${run_name}${sas_token}\" --recursive"
    echo ${cmd}
    eval "${cmd}"
    # Copy run flag
    touch "${finish_copy_flag}"
    cmd="azcopy copy \"${finish_copy_flag}\" \"${azure_input}/\
${run_name}/${run_name}${sas_token}\""
    echo ${cmd}
    eval "${cmd}"
}

main(){
    echo "Hello this is main"
    pa_check_mount "${run_mount}"
    pa_check_directory "${run_path}/${run_name}"
    while true
    do
        if pa_check_file_is_finished "${copy_flag}" "${copy_location}" 
        then
            sleep 15m
            copy_data_to_azure
            return
        else
            echo "Still waiting"
            sleep 5m
        fi
    done
}

# Execute the script
main
